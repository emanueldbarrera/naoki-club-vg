﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDetectorHelper : MonoBehaviour {

    [SerializeField] BanditController bandit;

    // Start is called before the first frame update
    void Start() {
        
    }

    // Update is called once per frame
    void Update() {
        
    }

    void OnTriggerStay2D(Collider2D collider) {
        if (collider.CompareTag("Player")) {
            bandit.OnPlayerStay();
        }
    }
}
