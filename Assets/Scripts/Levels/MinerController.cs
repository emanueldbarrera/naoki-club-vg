﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MinerController : MonoBehaviour {

    [Serializable] class Config {
        public Rigidbody2D rigidbody2D;
		public Animator animator;
        public float runningSpeed;
    }

    [Serializable] class State {
        public bool isMoving = false;
        public int direction = 1;
        public bool shouldMove = true;
    }

    [Serializable] class BookKeeping {
        public int previousDirection = 1;
    }

    [SerializeField] Config config;
    [SerializeField] State state;
    [SerializeField] BookKeeping bookKeeping;

    // Start is called before the first frame update
    void Start() {
        config.rigidbody2D = GetComponent<Rigidbody2D>();
        config.animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void FixedUpdate() {
        if (!state.shouldMove) return;
        if (!state.isMoving) {
            if (ShouldMove()) {
                config.rigidbody2D.velocity = new Vector2(config.runningSpeed * state.direction, config.rigidbody2D.velocity.y);
                state.isMoving = true;
                StartCoroutine(Stop());
            } else {
                config.rigidbody2D.velocity = new Vector2(0, config.rigidbody2D.velocity.y);
            }
        }
    }

    void Update() {

    }

    void LateUpdate() {
		Animate();
	}
 
	void Animate() {
		if (state.direction != bookKeeping.previousDirection) {
			bookKeeping.previousDirection = state.direction;
			transform.Rotate(new Vector3(0, state.direction*180, 0));
		}
        if (state.isMoving) {
            config.animator.SetBool("isMoving", true);
        } else {
            config.animator.SetBool("isMoving", false);
        }
    }

    public bool ShouldMove() {
        float probability = UnityEngine.Random.value;
		if (probability < 0.01) {
			return true;
		}
        return false;
    }

    IEnumerator Stop() {
        yield return new WaitForSeconds(2);
		state.isMoving = false;
        state.direction = state.direction * -1;
    }

    public void StopForever() {
        state.shouldMove = false;
        state.isMoving = false;
        config.rigidbody2D.velocity = Vector2.zero;
    }
}
