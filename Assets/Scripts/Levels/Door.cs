﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour {

    [SerializeField] GameObject doorObject;
    [SerializeField] Transform doorEndPosition;
    [SerializeField] float closingSpeed = 20f;
    bool shouldCloseDoor = false;

    void Start() {
        
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.L)) {
            shouldCloseDoor = true;
        }
        if (shouldCloseDoor) {
            float step = closingSpeed * Time.deltaTime;
		    doorObject.transform.position = Vector3.MoveTowards(doorObject.transform.position, doorEndPosition.position, step);
            if (doorObject.transform.position.y < doorEndPosition.position.y) {
                shouldCloseDoor = false;
            }
        }
    }

    public void CloseDoor() {
        shouldCloseDoor = true;
    }

    public void CloseDoorWithDelay() {
        StartCoroutine(ScheduleCloseDoor());
    }

    IEnumerator ScheduleCloseDoor() {
        yield return new WaitForSeconds(1);
        CloseDoor();
    }
}
