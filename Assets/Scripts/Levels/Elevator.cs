﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elevator : MonoBehaviour {

    float speed = 2f;
    Vector2 lower = new Vector2(1038.69f, -25.2f);
    Vector2 upper = new Vector2(1038.69f, -9.5f);
    Vector2 target;

    // Start is called before the first frame update
    void Start() {
        target = upper;
    }

    // Update is called once per frame
    void Update() {
        if (transform.position.y >= upper.y && transform.position.y >= lower.y) {
            target = lower;
        }
        if (transform.position.y <= upper.y && transform.position.y <= lower.y) {
            target = upper;
        }
		float step = speed * Time.deltaTime;
        transform.position = Vector2.MoveTowards(transform.position, target, step);
    }
}
