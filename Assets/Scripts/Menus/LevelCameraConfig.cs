﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable] public class LevelCameraConfig {

    public SpriteRenderer limits;
    public float initialPositionX;
    public float initialPositionY;
    public int fieldOfView;

}
