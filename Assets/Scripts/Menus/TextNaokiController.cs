﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextNaokiController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		StartCoroutine (Animate());	
	}
	
	// Update is called once per frame
	void Update () {
	}

	IEnumerator Animate() {
		while (true) {
			int waitTime = Random.Range (2, 5);
			yield return new WaitForSecondsRealtime (waitTime);
			gameObject.GetComponent<Animator> ().SetTrigger ("shine");
		}
	}
}
