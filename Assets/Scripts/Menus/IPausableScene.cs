﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public interface IPausableScene : IEventSystemHandler {

	void UnpauseScene();

}
