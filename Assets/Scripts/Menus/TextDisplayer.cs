﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

public class TextDisplayer : MonoBehaviour {

	[SerializeField] SpriteAtlas atlas;
	[SerializeField] string atlasIndexName;
	[SerializeField] string atlasIndexStrings;
	[SerializeField] int orderInLayer = 0;
	List<GameObject> characters = new List<GameObject>();
	string lastString = "";

	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void FixedUpdate () {
		if (atlasIndexStrings != "" && !lastString.Equals(atlasIndexStrings)) {
			// Remove previous characters
			for (int i = 0; i < characters.Count; i++) {
				Destroy (characters [i]);
			}
			characters = new List<GameObject>();
			// Add new characters
			float xLength = this.gameObject.transform.position.x;
			for (int i = 0; i < atlasIndexStrings.Length; i++) {
				string atlasIndexOffset = atlasIndexStrings.Substring (i, 1);
				GameObject character = new GameObject();
				character.transform.parent = this.gameObject.transform;
				SpriteRenderer spriteRenderer = character.AddComponent<SpriteRenderer>();
				spriteRenderer.sprite = atlas.GetSprite (atlasIndexName + atlasIndexOffset);
				spriteRenderer.sortingOrder = orderInLayer;
				character.transform.position = new Vector2(xLength, this.gameObject.transform.position.y);
				xLength += spriteRenderer.bounds.size.x;
				character.transform.localScale = new Vector3(1f, 1f, 1f);
				characters.Add (character);
			}
			lastString = atlasIndexStrings;
		}
	}
}
