﻿using System.Collections;
using UnityEngine;
using System;

public class CentipedeController : MonoBehaviour {

	[Serializable] class Config {
		public int maxLife = 10;
		public GameObject spitPrefab;
		public Animator animator;
		public GameManager gameManager;
		public GameObject player;
		public CentipedeTail tail;
		public Animator animatorRocks;
		public GameObject wall;
	}

	[Serializable] class State {
		public int life;
		public bool isDamaged = false;
		public bool isUnavoidableDamaged = false;
		public bool shouldEmerge = false;
		public bool shouldAttack = false;
		public bool lastAttackWasSpit = false;
		public bool shouldSpit = false;
		public bool canReceiveDamage = true;
		public bool isDead = false;
		public bool hasWon = false;
		public bool shouldSlash = false;
		public bool isSlashing = false;
		public bool shouldBite = false;
		public bool isBiting = false;
		public bool nextAttackIsHardcore = false;
	}

	[SerializeField] Config config;
	[SerializeField] State state;

	void Start () {
		config.animator = GetComponent<Animator> ();
		state.life = config.maxLife;
	}
	
	void Update () {
		if (state.isDead || state.hasWon) {
			return;
		}
		if (state.shouldSlash) {
			state.shouldSlash = false;
			state.isSlashing = true;
			state.shouldBite = false;
			state.isBiting = false;
			return;
		}
		if (state.shouldBite) {
			state.shouldBite = false;
			state.isBiting = true;
			state.shouldSlash = false;
			state.isSlashing = false;
			return;
		}
		if (state.shouldAttack) {
			Attack();
			state.shouldAttack = false;
		}
	}

	void LateUpdate() {
		Animate();
	}

	void Attack() {
		if (state.lastAttackWasSpit) {
			config.tail.Emerge(config.player.transform.position.x);
		} else {
			state.shouldSpit = true;
			state.canReceiveDamage = false;
		}
		state.lastAttackWasSpit = !state.lastAttackWasSpit;
	}

	public void SpitObject () {
		GameObject spitObject = Instantiate (config.spitPrefab);
		spitObject.transform.parent = transform.parent.transform;
		spitObject.transform.localPosition = new Vector2(1.6f, 5.6f);
		Vector3 target = config.player.transform.position;
		spitObject.transform.right = target - spitObject.transform.position;
		spitObject.GetComponent<CentipedeSpit>().SetTarget(target);
	}

	void Animate() {
		if (state.isDead) {
			config.animator.SetBool("isDead", true);
			config.animatorRocks.SetTrigger("off");
			return;
		}
		if (state.hasWon) {
			config.animator.SetBool("hasWon", true);
			return;
		}
		if (state.isUnavoidableDamaged) {
			config.animator.SetTrigger("isUnavoidableDamaged");
			state.isUnavoidableDamaged = false;
			state.isSlashing = false;
			state.isBiting = false;
			state.nextAttackIsHardcore = true;
		}
		config.animator.SetBool("slash", state.isSlashing);
		config.animator.SetBool("bite", state.isBiting);
		if (state.isDamaged) {
			config.animator.SetTrigger("isDamaged");
			state.isDamaged = false;
			return;
		}
		if (state.shouldEmerge) {
			state.shouldEmerge = false;
			config.animator.SetTrigger("emerge");
			config.animatorRocks.SetTrigger("on");
		}
		if (state.shouldSpit) {
			state.shouldSpit = false;
			config.animator.SetTrigger("spit");
		}
	}

	public void Emerge() {
		state.shouldEmerge = true;
	}

	public void NotifyEndOfEmerge() {
		config.gameManager.EndOfEmerge();
		StartCoroutine(ScheduleAttack());
	}

	public void NotifyEndOfAttack() {
		StartCoroutine(ScheduleAttack());
		state.canReceiveDamage = true;
	}

	IEnumerator ScheduleAttack() {
		yield return new WaitForSeconds(3);
		state.shouldAttack = true;
	}

	public void Slash() {
		state.shouldSlash = true;
		state.canReceiveDamage = false;
		FXPlayer.MyRef.Play("slashCut");
	}

	public void EndSlash() {
		state.isSlashing = false;
		state.canReceiveDamage = true;
	}

	public void Bite() {
		state.shouldBite = true;
		state.canReceiveDamage = false;
		FXPlayer.MyRef.Play("bite");
	}

	public void EndBite() {
		state.isBiting = false;
		state.canReceiveDamage = true;
	}

	void ReduceLife() {
		state.life--;
		if (state.life <= 0) {
			Die();
		}
	}

	void Die() {
		state.isDead = true;
		Destroy(config.wall);
		config.tail.ShouldSumergeForever();
		FXPlayer.MyRef.Play("moan");
	}

	void Win() {
		state.hasWon = true;
		Destroy(config.wall);
		Destroy(config.tail);
	}

	public void ReceiveDamage() {
		if (state.canReceiveDamage && !state.isDamaged && !state.isUnavoidableDamaged) {
			state.isDamaged = true;
		}
	}

	public void ReceiveUnavoidableDamage() {
		if (!state.isDamaged && !state.isUnavoidableDamaged) {
			state.isUnavoidableDamaged = true;
		}
	}

	public void DecreaseLife() {
		FXPlayer.MyRef.Play("centipedeDamage");
		int damage = 1;
		if (state.nextAttackIsHardcore) {
			state.nextAttackIsHardcore = false;
			damage = 10;
		}
		state.life = state.life - damage;
		if (state.life <= 0) {
			Die();
		}
	}

	public void ShowVictoryMessage() {
		config.gameManager.ShowVictoryMessage();
	}
}
