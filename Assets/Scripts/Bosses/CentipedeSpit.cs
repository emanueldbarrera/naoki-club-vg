﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;

public class CentipedeSpit : MonoBehaviour {

	float speed = 30f;
	float speedSlowRatio = 2f;
	Vector3 direction;
	bool shouldMoveSlow = false;

	// Use this for initialization
	void Start () {
		FXPlayer.MyRef.Play("tirarAcido");
		DestroyObject(4);
	}
	
	// Update is called once per frame
	void Update () {
		float speedSlowRatio = 1f;
		if (shouldMoveSlow) {
			speedSlowRatio = this.speedSlowRatio;
		}
		transform.position += (direction * Time.deltaTime / speedSlowRatio);
	}

	public void SetTarget(Vector3 target) {
		direction = (target - transform.position).normalized * speed;
	}

	public void DestroyObject(int seconds) {
		if (seconds == 0) {
			Destroy(gameObject);
			return;
		}
		StartCoroutine(ScheduleDestroy(seconds));
	}

	IEnumerator ScheduleDestroy(int seconds) {
		yield return new WaitForSeconds(2);
		Destroy(gameObject);
	}

	void OnTriggerEnter2D(Collider2D collider) {
		if (collider.CompareTag("Player")) {
			GetComponent<Animator>().SetTrigger("destroy");
			GetComponent<CapsuleCollider2D>().enabled = false;
			shouldMoveSlow = true;
		}
	}
}
