﻿using UnityEngine;
using System;
using Random = UnityEngine.Random;

public class babycentipede_controller : MonoBehaviour
{
    [Serializable] class ConfigData {
        public Animator animator;
        public Rigidbody2D rigidbody2D;
        public GameObject target;
        public GameObject morderDamager;
        public SpriteRenderer sprite;
        public float centripedespeed = 1.2f;
    }

    [Serializable] class StateData
    {
        public int direction = -1;
        public bool patrullando = true;
        public float coordenadaXizqPatrullar;
        public float coordenadaXderPatrullar;
        public bool isAttacking = false;
        public bool isRunningforAttack = false;
        public float NaokiPosition = 0;
        public float TargetPositionX = 0;
        public bool isRolling = false;
        public bool isAskedNaokiLastPosition = false;
        public float NaokiLastPositonX = 0;
        public bool rodarPrincipio = false;
        public bool rodarLoopEnd = false;
        public bool atacaporladerecha = false;
        public bool reciviendoDaño = false;
        public bool isDead = false;
    }

    [SerializeField] ConfigData config;
    [SerializeField] StateData state;

    void Start()
    {
        config.animator = GetComponent<Animator>();
        config.rigidbody2D = GetComponent<Rigidbody2D>();
        config.sprite = GetComponent<SpriteRenderer>();
        config.target = GameObject.FindGameObjectWithTag("Player");
    }

    void Update()
    {
        if (state.reciviendoDaño || state.isDead)
        {
            return;
        }

        // patrulla ->
        float step = config.centripedespeed * Time.deltaTime;

        if (state.patrullando && state.direction == -1)
        {
            transform.position = Vector2.MoveTowards(transform.position, new Vector2(state.coordenadaXizqPatrullar, transform.position.y), step);
        }
        else if (state.patrullando && state.direction == 1)
        {
            transform.position = Vector2.MoveTowards(transform.position, new Vector2(state.coordenadaXderPatrullar, transform.position.y), step);
        }
        //<- patrulla


        // Cambiar direccion y flipSprite ->
        if (transform.position.x <= state.coordenadaXizqPatrullar || transform.position.x >= state.coordenadaXderPatrullar)
        {
            state.direction *= -1;
            config.rigidbody2D.velocity = new Vector2(0, 0);
        }
        
        if (state.atacaporladerecha && !state.patrullando)
        {
            state.direction = 1;
        }
        else if (!state.patrullando)
        {
            state.direction = -1;
        }

        
        if (state.patrullando)
        {
            config.morderDamager.SetActive(false);

            if (state.direction == 1)
            {
                config.sprite.flipX = true;
            }
            else
            {
                config.sprite.flipX = false;
            }
        }
        // <- Cambiar direccion y flipSprite


        //Pregunta la ultima posición de Naoki en X antes que salte
        if (Math.Abs(config.target.transform.position.y - transform.position.y) < 2)
        {
            state.NaokiLastPositonX = config.target.transform.position.x;
        }
        else
        {
            state.rodarLoopEnd = true;
        }

        //Si esta a cierta distancia va a correr hacia donde está Naoki
        if (Math.Abs(config.target.transform.position.x - transform.position.x) <= 20 && Math.Abs(config.target.transform.position.y - transform.position.y) <= 3 && config.target.transform.position.x - 2 <= state.coordenadaXderPatrullar && config.target.transform.position.x >= state.coordenadaXizqPatrullar + 2 && config.target.transform.position.y >= transform.position.y)
        {        
            RunningforAttack();
        }
        else
        {
            state.isAttacking = false;
            state.isRunningforAttack = false;
            state.patrullando = true;
            state.rodarPrincipio = false;
            state.isRolling = false;
        }


        if (Math.Abs(config.target.transform.position.x - transform.position.x) <= 3 && Math.Abs(config.target.transform.position.y - transform.position.y) <= 3)
        {
            state.rodarLoopEnd = true;
            state.isAttacking = true;
            config.morderDamager.SetActive(true);

            if (state.direction == 1)
            {
                config.morderDamager.transform.localPosition = new Vector3(1, 0, 0);
            }
            else
            {
                config.morderDamager.transform.localPosition = new Vector3(-1, 0, 0);
            }
        }
        else if (Math.Abs(config.target.transform.position.y - transform.position.y) <= 2)
        {
            state.rodarLoopEnd = false;
            state.isAttacking = false;
            config.morderDamager.SetActive(false);
        }
    }
    
    void AnimationRollingStarts()
    {
        config.centripedespeed *= 2;
        print("velocidad duplicada");
    }

    void AnimationRollingEnds()
    {
        config.centripedespeed /= 2;
        print("velocidad normalizada");
 
        int probabilidad = Random.Range(0, 2); // Probabilidad del 50% de que el bicho rebote

        if (probabilidad == 1 && Math.Abs(config.target.transform.position.y - transform.position.y) <= 2 && Math.Abs(config.target.transform.position.x - transform.position.x) <= 3)
        {
            config.rigidbody2D.AddForce(new Vector3(-1 * state.direction, 0.5f , 0) * 30, ForceMode2D.Impulse);
        }
        else if (Math.Abs(config.target.transform.position.y - transform.position.y) <= 2 && Math.Abs(config.target.transform.position.x - transform.position.x) <= 3)
        {
            config.rigidbody2D.AddForce(new Vector3(-1 * state.direction, 0.5f, 0) * 10, ForceMode2D.Impulse);
        }
    }



    void RunningforAttack()
    {
        if (state.reciviendoDaño || state.isDead)
        {
            return;
        }

        print("RunningforAttack");

        config.morderDamager.SetActive(false);

        float step = config.centripedespeed * Time.deltaTime * 6;

        state.isRunningforAttack = true;
        state.patrullando = false;

        if (config.target.transform.position.x > transform.position.x && !state.patrullando) //ataca por la der
        {
            state.atacaporladerecha = true;
            config.sprite.flipX = true;
            transform.position = Vector2.MoveTowards(transform.position, new Vector2(config.target.transform.position.x - 2, transform.position.y), step);
        }
        else if (config.target.transform.position.x < transform.position.x && !state.patrullando) //ataca por la izq
        {
            state.atacaporladerecha = false;
            config.sprite.flipX = false;
            transform.position = Vector2.MoveTowards(transform.position, new Vector2(config.target.transform.position.x + 2, transform.position.y), step);
        }
    }

    void ReceiveDamage()
    {
        state.reciviendoDaño = true;
        print("reciviendo daño");

        config.rigidbody2D.AddForce(new Vector3(-1 * state.direction, 0.5f, 0) * 10, ForceMode2D.Impulse);
        
    }

    void EndReceiveDamage()
    {
        state.reciviendoDaño = false;
        config.centripedespeed = 1.2f;
    }

    void Die()
    {
        state.isDead = true;
        print("die");
    }

    void EndDie()
    {
        print("already die (terminó animación)");
        state.isDead = false;
    }

    private void LateUpdate()
    {
        Animate();
    }

    void Animate()
    {
        config.animator.SetBool("isAttacking", state.isAttacking);
        config.animator.SetBool("IsRunningforAttack", state.isRunningforAttack);
        config.animator.SetBool("Rodar1", state.rodarPrincipio);
        config.animator.SetBool("RodarLoopEnd", state.rodarLoopEnd);
        config.animator.SetBool("isPatrullando", state.patrullando);
        config.animator.SetBool("ReciviendoDaño", state.reciviendoDaño);
        config.animator.SetBool("IsDead", state.isDead);
    }
} 