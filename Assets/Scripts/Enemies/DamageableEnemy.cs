﻿using UnityEngine;

public class DamageableEnemy : MonoBehaviour {

	[SerializeField] Transform enemy;

	[SerializeField] int health;
	[SerializeField] bool canRechargePowerBar = false;
	[SerializeField] bool canReceiveDamageNoMatterWhat = false;

	void Start () {
		
	}
	
	void Update () {
		if (health > 0) {
			return;
		}
		Die();
	}

	public void OnTriggerEnter2D(Collider2D collider){
		if (collider.gameObject.CompareTag("shuriken")) {
			ReceiveDamage(1, false);
			collider.gameObject.SendMessage("DestroyAndPlaySound");
		} else if (collider.gameObject.CompareTag("slash")) {
			Debug.Log("slash");
			ReceiveDamage(2, false);
        	FXPlayer.MyRef.Play("slash");
		} else if (collider.gameObject.CompareTag("special-attack")) {
			ReceiveDamage(15, canReceiveDamageNoMatterWhat);
		}
	}

	void ReceiveDamage(int damage, bool shouldReceiveDamageNoMatterWhat) {
		if (canRechargePowerBar) {
			PowerBarController.MyRef.IncreasePowerAmount(7);
		}
		health = health - damage;
		if (health > 0) {
			if (shouldReceiveDamageNoMatterWhat) {
				enemy.SendMessage("ReceiveUnavoidableDamage");
			} else {
				enemy.SendMessage("ReceiveDamage");
			}
		}
	}

	void Die(){
		enemy.SendMessage("Die");
		gameObject.SetActive(false);
	}
}
