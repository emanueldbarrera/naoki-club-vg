﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamagerColliderManager : MonoBehaviour {

    public GameObject[] collidersHit1;
    public GameObject[] collidersHit2;

    public void SetActiveCollider(string hitAndColliderNumber) {
        int hit = int.Parse(hitAndColliderNumber.Substring(0, 1));
        int colliderNumber = int.Parse(hitAndColliderNumber.Substring(2));
        GameObject[] collidersArray;
        if (hit == 1) {
            collidersArray = collidersHit1;
        } else {
            collidersArray = collidersHit2;
        }
        if (colliderNumber < collidersArray.Length) {
            collidersArray[colliderNumber].SetActive(true);
        }
        if (colliderNumber != 0) {
            collidersArray[colliderNumber-1].SetActive(false);
        }
        if (colliderNumber == collidersArray.Length) {
            collidersArray[colliderNumber].SetActive(false);
        }
    }
}
