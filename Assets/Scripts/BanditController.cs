﻿using System.Collections;
using UnityEngine;
using System;
using DG.Tweening;

public class BanditController : MonoBehaviour {

    public const int RIGHT = 1;
    public const int LEFT = -1;
    public const int HIT_GROUND = 1;
    public const int HIT_INVERT = 2;

    [Serializable] class ConfigData {
        public Rigidbody2D rigidbody2D;
		public Animator animator;
        public SpriteRenderer spriteRenderer;
        public GameObject player;
        public GameObject damager;
        public GameObject damagerThrower;
        public GameObject damagerColliders;
        public GameObject damagerThrowerColliders;
        public GameManager gameManager;
        public float walkSpeed = 5f;
        public float runningSpeedMultiplicator = 2f;
        public float patrolLowerLimit = 750f;
        public float patrolUpperLimit = 850f;
        public float gapBetweenBanditAndPlayer = 3f;
    }

    [Serializable] class StateData {
        public int direction = RIGHT;
        public bool shouldPatrol = true;
        public bool isPatrolling = false;
        public bool isWalking = false;
        public bool shouldRunWhileWalking = true;
        public float WalkingTargetX;
        public float currentPatrolLimit;
        public bool shouldRunAndHit = false;
        public bool isRunningAndHitting = false;
        public float runAndHitTargetX;
        public bool isHitting = false;
        public int hit;
        public bool isDamaged = false;
        public bool isDead = false;
        public bool isResting = false;
    }

    [Serializable] class BookKeeping {
        public float previousXPosition;
        public int previousDirection;
        public float previousPatrolLimit;
    }

	[SerializeField] ConfigData config;
	[SerializeField] StateData state;
	[SerializeField] BookKeeping bookKeeping;

    void Start() {
		config.rigidbody2D = GetComponent<Rigidbody2D>();
        config.animator = GetComponent<Animator>();
        config.spriteRenderer = GetComponent<SpriteRenderer>();
        state.isWalking = true;
        state.WalkingTargetX = 980f;
        bookKeeping.previousPatrolLimit = config.patrolLowerLimit;

        // Deactivate damagers
        config.damager.SetActive(false);
        config.damagerThrower.SetActive(false);
    }

    void FixedUpdate() {
    }

    void Update() {
        if (state.isDamaged || state.isDead || state.isResting) {
            return;
        }

        if (state.shouldRunAndHit) {
            InitRunAndHit();
        }
        // Detect end of RunAndHit state
        if (state.isRunningAndHitting && !state.isWalking) {
            EndRunAndHit();
        }

        if (state.isWalking) {
            Walk();
        }

        if (!state.isRunningAndHitting && !state.isWalking && !state.isHitting) {
            state.shouldPatrol = true;
        }

        if (state.shouldPatrol) {
            InitPatrol();
        }
        if (state.isPatrolling) {
            CheckPatrol();
        }
    }

    void LateUpdate() {
        Animate();
        // Preserve value of position
        bookKeeping.previousXPosition = transform.position.x;
        bookKeeping.previousDirection = state.direction;
    }

    void Animate() {
        config.animator.SetBool("isDamaged", state.isDamaged);
        config.animator.SetBool("isDead", state.isDead);
        config.animator.SetBool("isWalking", state.isWalking);
        config.animator.SetInteger("isHitting", state.isHitting ? state.hit : 0);
    }

    void Walk() {
        // Walk a small amount
        float step = config.walkSpeed * Time.deltaTime;
        if (state.shouldRunWhileWalking) step *= config.runningSpeedMultiplicator;
		transform.position = Vector2.MoveTowards(
            transform.position,
            new Vector2(state.WalkingTargetX, transform.position.y),
            step
        );
        if (
            Mathf.Max(bookKeeping.previousXPosition, transform.position.x, state.WalkingTargetX) == transform.position.x ||
            Mathf.Min(bookKeeping.previousXPosition, transform.position.x, state.WalkingTargetX) == transform.position.x
        ) {
            state.isWalking = false;
        }
        Rotate();
    }

    void Rotate() {
        if (transform.position.x != bookKeeping.previousXPosition) {
            state.direction = transform.position.x <= bookKeeping.previousXPosition ? LEFT : RIGHT;
            if (state.direction != bookKeeping.previousDirection) {
                transform.Rotate(new Vector3(0, state.direction * 180, 0));
            }
        }
    }

    void InitPatrol() {
        // Choose a direction
        state.currentPatrolLimit = 
            bookKeeping.previousPatrolLimit == config.patrolLowerLimit ?
            config.patrolUpperLimit : config.patrolLowerLimit;

        // Init walking
        state.shouldPatrol = false;
        state.isPatrolling = true;
        state.isWalking = true;
        state.shouldRunWhileWalking = false;
        state.WalkingTargetX = state.currentPatrolLimit;
        bookKeeping.previousPatrolLimit = state.currentPatrolLimit;
    }

    void CheckPatrol() {
        if (!state.isWalking)  {
            state.isPatrolling = false;
            state.shouldPatrol = true;
        }
    }

    void EndPatrol() {
        state.shouldPatrol = false;
        state.isPatrolling = false;
    }

    void InitHit() {
        FXPlayer.MyRef.Play("hit");
        state.isHitting = true;
        state.isWalking = false;
        state.hit = (int)UnityEngine.Random.Range(HIT_GROUND, HIT_INVERT + 1);
        if (state.hit == HIT_GROUND) config.damager.SetActive(true);
        else if (state.hit == HIT_INVERT) config.damagerThrower.SetActive(true);
    }

    public void EndHit() {
        state.isHitting = false;
        config.damager.SetActive(false);
        config.damagerThrower.SetActive(false);
        state.isResting = true;
        if (state.hit == HIT_GROUND) {
            ShakeGround();
            StartCoroutine(Rest(1.5f));
        } else {
            StartCoroutine(Rest(1f));
        }
    }

    IEnumerator Rest(float seconds){
        yield return new WaitForSeconds(seconds);
        state.isResting = false;
    }

    void ShakeGround() {
        config.gameManager.ShakeWorld(2, true);
    }

    void InitRunAndHit() {
        EndPatrol();
        state.shouldRunAndHit = false;
        state.isRunningAndHitting = true;
        float playerPositionX = config.player.transform.position.x;
        // We want to start smashing the player a couple of centimeters before its actual position
        float gapBetweenBanditAndPlayer = (playerPositionX < transform.position.x) ? 
            config.gapBetweenBanditAndPlayer : -config.gapBetweenBanditAndPlayer;
        float walkTarget = config.player.transform.position.x + gapBetweenBanditAndPlayer;
        // ...Unless we were actually pretty close
        if (
            Mathf.Min(playerPositionX, transform.position.x, walkTarget) == walkTarget ||
            Mathf.Max(playerPositionX, transform.position.x, walkTarget) == walkTarget        
        ) {
            walkTarget = playerPositionX;
        }
        state.WalkingTargetX = walkTarget;
        state.isWalking = true;
        state.shouldRunWhileWalking = true;
    }

    void EndRunAndHit() {
        state.isRunningAndHitting = false;
        InitHit();
    }

    public void OnPlayerStay() {
        if (!state.isRunningAndHitting && !state.isHitting && !state.isResting) {
            state.shouldRunAndHit = true;
        }
    }

    public void OnPlayerStayVeryClose() {
        if (state.isRunningAndHitting && !state.isDamaged && !state.isHitting) {
            EndRunAndHit();
        }
    }

    public void ReceiveDamage() {
        if (state.isDead || state.isDamaged) {
            return;
        }
        if (state.isHitting) {
            state.isHitting = false;
            config.damager.SetActive(false);
            config.damagerThrower.SetActive(false);
        }
        state.isDamaged = true;
    }

    public void EndReceiveDamage() {
        state.isDamaged = false;
    }

    public void Die() {
        state.isDead = true;

        // Deactivate damagers   
        config.damager.SetActive(false);
        config.damagerThrower.SetActive(false);
        config.damagerColliders.SetActive(false);
        config.damagerThrowerColliders.SetActive(false);
    }
}
