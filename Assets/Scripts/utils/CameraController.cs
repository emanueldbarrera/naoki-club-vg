﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using Cinemachine;

public class CameraController : MonoBehaviour {
	
	[Serializable] class ConfigData {
		public CinemachineVirtualCamera virtualCamera;
		public CinemachineFramingTransposer vcamTransposer;
		// Shake
		public float shakeMinX = 0.4f;
		public float shakeMaxX = 0.55f;
		public float shakeMinY = 0.45f;
		public float shakeMaxY = 0.75f;
	}

	[Serializable] class StateData {
		public bool isShaking = false;
	}
	
	[SerializeField] public LevelCameraConfig[] levelCameraConfig;
	[SerializeField] public int levelCameraConfigActive;
	[SerializeField] ConfigData config;
	[SerializeField] StateData state;

	void Start () {
		levelCameraConfigActive = 0;
		config.vcamTransposer = config.virtualCamera.GetCinemachineComponent<CinemachineFramingTransposer>();
		config.vcamTransposer.m_ScreenX = levelCameraConfig[levelCameraConfigActive].initialPositionX;
		config.vcamTransposer.m_ScreenY = levelCameraConfig[levelCameraConfigActive].initialPositionY;
		// SetLevelCameraConfigActive(1); // DEBUGGG
	}

	void Update() {
		if (state.isShaking) {
			config.vcamTransposer.m_ScreenX = UnityEngine.Random.Range(config.shakeMinX, config.shakeMaxX);
			config.vcamTransposer.m_ScreenY = UnityEngine.Random.Range(config.shakeMinY, config.shakeMaxY);	
		}
	}
	
	void LateUpdate () {
		float minX = levelCameraConfig[levelCameraConfigActive].limits.transform.position.x;
		float maxY = levelCameraConfig[levelCameraConfigActive].limits.transform.position.y;
		float maxX = levelCameraConfig[levelCameraConfigActive].limits.transform.position.x + levelCameraConfig[levelCameraConfigActive].limits.bounds.size.x;
		float minY = levelCameraConfig[levelCameraConfigActive].limits.transform.position.y - levelCameraConfig[levelCameraConfigActive].limits.bounds.size.y;

		transform.position = new Vector3 (
			transform.position.x < minX ? minX : transform.position.x > maxX ? maxX : transform.position.x,
			transform.position.y < minY ? minY : transform.position.y > maxY ? maxY : transform.position.y,
			transform.position.z
		);
	}

	public void Shake() {
		state.isShaking = true;
	}

	public void EndShake() {
		state.isShaking = false;
		config.vcamTransposer.m_ScreenX = levelCameraConfig[levelCameraConfigActive].initialPositionX;
		config.vcamTransposer.m_ScreenY = levelCameraConfig[levelCameraConfigActive].initialPositionY;
	}

	public void SetLevelCameraConfigActive(int levelCameraConfigActive) {
		this.levelCameraConfigActive = levelCameraConfigActive;
		config.vcamTransposer.m_ScreenX = levelCameraConfig[levelCameraConfigActive].initialPositionX;
		config.vcamTransposer.m_ScreenY = levelCameraConfig[levelCameraConfigActive].initialPositionY;
		config.virtualCamera.m_Lens.FieldOfView = levelCameraConfig[levelCameraConfigActive].fieldOfView;
	}

	public void UpdateLevelCameraConfigActive(LevelCameraConfig levelCameraConfig) {
		config.vcamTransposer.m_ScreenX = levelCameraConfig.initialPositionX;
		config.vcamTransposer.m_ScreenY = levelCameraConfig.initialPositionY;
		config.virtualCamera.m_Lens.FieldOfView = levelCameraConfig.fieldOfView;
	}
}
