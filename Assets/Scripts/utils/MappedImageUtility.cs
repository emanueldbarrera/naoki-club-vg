using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;

public class MappedImageUtility : MonoBehaviour {

	[SerializeField] SpriteAtlas atlas;
	[SerializeField] Image imageRenderer;
	[SerializeField] string atlasIndexName;
	[SerializeField] string atlasIndexNumber;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (atlasIndexNumber != "") {
            imageRenderer.overrideSprite = atlas.GetSprite (atlasIndexName + atlasIndexNumber);
		}
	}

	public void SetIndexNumber(int indexNumber) {
		atlasIndexNumber = indexNumber.ToString ();
	}
}