﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxController : MonoBehaviour {

    [SerializeField] bool shouldDestroyBox = false;

    // Start is called before the first frame update
    void Start() {
        
    }

    // Update is called once per frame
    void Update() {
        
    }

    public void Die() {
        GetComponent<Animator>().SetTrigger("disappear");
        if (shouldDestroyBox) {
            FXPlayer.MyRef.Play("box1");
        } else {
            FXPlayer.MyRef.Play("box2");
        }
    }

    public void DoDestroy() {
        if (shouldDestroyBox) Destroy(gameObject);
    }
}
