﻿using System.Collections;
using UnityEngine;
using System;
using DG.Tweening;

public class NaokiController : MonoBehaviour {

    [Serializable] class ConfigData {
		public GameManager gameManager;
		public Rigidbody2D rigidbody2D;
		public Animator animator;
		public Animator armAnimator;
		public LayerMask layerGround;
		public GameObject shurikenPrephab;
		public GameObject slashDamagePrephab;
        public GameObject SpecialAttackDamager;
        public PowerBarController powerBarController;
		public float runningSpeed = 10f;
		public float jumpSpeed = 1200f;
		public float damageSpeedX = 300f;
		public float damageSpeedXMultiplicator = 5;
		public float damageSpeedY = 500f;
		public float dashSpeed = 600f;
		public float shurikenSpeed = 50f;
		public float slideSpeed = 1.2f;
        public float slideSpeedToChangeAnimation = 0.3f;
		// Wall Jump
        public float raycastdistanceawall = 1f;
        public float forcejumpwallx = 600;
        public float forcejumpwally = 650;
        public float timerwallinputaferjump = 0.2f;
    }

	[Serializable] class StateData {
		public int direction = 1;
		public bool isRunning = false;
		public bool onGround = false;
		public bool isJumping = false;
		public bool shouldJump = false;
		public bool isDoubleJumping = false;
		public bool shouldDoubleJump = false;
		public bool isReceivingDamage = false;
		public bool shouldReceiveDamage = false;
        public bool shouldBeThrownAway = false;
		public bool isDashing = false;
		public bool shouldDash = false;
		public bool isEnabledToDash = false;
		public bool isThrowingShuriken = false;
		public bool shouldThrowShuriken = false;
		public bool isSlashing = false;
		public int slashToApply = 1;
		public GameObject slashDamage;
		public bool isDead = false;
		public bool isControllable = true;
        public bool isEnabledToSlide = true;
		public bool isSliding = false;
		public bool shouldStopSliding = false;
		public float currentSlideSpeed = 0.1f;
		public Vector3 slideEnd;
		public Tweener slideSpeedTween;
        public bool shouldDoSpecialAttack = false;
        public bool isDoingSpecialAttack = false;
        public bool shouldStickToWall = false;

		// Wall Jump
        public bool timerstart = false;
        public bool canjumponwall = false;
        public bool walljump = false;
        public bool joystick = false;
    }

	[Serializable] class BookKeepingData {
		public int previousDirection = 1;
        public bool wasOnGround = false;
	}

	[SerializeField] ConfigData config;
	[SerializeField] StateData state;
	[SerializeField] BookKeepingData bookKeeping;

	void Start () {
		config.rigidbody2D = GetComponent<Rigidbody2D>();

    }

	void FixedUpdate () {
        config.rigidbody2D.velocity = new Vector2(
            Mathf.Min(config.rigidbody2D.velocity.x, 25f),
            Mathf.Min(config.rigidbody2D.velocity.y, 25f)
        );

        if (state.isDead) {
            state.shouldReceiveDamage = false;
            state.isReceivingDamage = false;
            return;
        }

        if (state.isDoingSpecialAttack) {
            return;
        }

		if (state.isReceivingDamage || state.isSliding) {
			return;
		}
		if (state.shouldReceiveDamage) {
			config.rigidbody2D.velocity = Vector2.zero;
            if (state.shouldBeThrownAway) {
                state.shouldBeThrownAway = false;
                int throwDirection = config.gameManager.isLevel2Active ? 1 : state.direction;
                config.rigidbody2D.AddForce(new Vector2(config.damageSpeedX * -1 * throwDirection * config.damageSpeedXMultiplicator, config.damageSpeedY));
            } else {
                config.rigidbody2D.AddForce(new Vector2(config.damageSpeedX * -1 * state.direction, config.damageSpeedY));
            }
            config.gameManager.ReduceLife();
			state.shouldReceiveDamage = false;
			state.isReceivingDamage = true;
			state.isRunning = false;
			return;
		}

        if (state.shouldDash)
        {
            config.rigidbody2D.AddForce(new Vector2(config.dashSpeed * state.direction, 0));
            state.shouldDash = false;
            state.isDashing = true;
            FXPlayer.MyRef.Play("dash");
            return;
        }

        if (state.isDashing)
        {
            config.rigidbody2D.velocity = new Vector2(config.rigidbody2D.velocity.x, 0);
            return;
        }

        // pegar Naoki a la pared y que salte en dirección contraria >>
        // RaycastHit2D raycasthitpared = Physics2D.Raycast(transform.position, new Vector2(state.direction, 0), 15f);

        // Debug.Log(raycasthitpared.collider.tag);
        // if (raycasthitpared.collider != null && raycasthitpared.collider.CompareTag("wall") && !state.walljump && config.rigidbody2D.velocity.y < 0) {
        if (state.shouldStickToWall && !state.walljump && state.isRunning && state.isControllable && !state.onGround) {
            config.rigidbody2D.velocity = new Vector2(0, 0);
            config.rigidbody2D.gravityScale = 0;
            state.isJumping = false;
            state.isDoubleJumping = false;
            state.canjumponwall = true;
            return;
        } else {
            config.rigidbody2D.gravityScale = 1;
            state.canjumponwall = false;
        }
        // }
        // else
        // {
        //     state.canjumponwall = false;
        //     config.rigidbody2D.gravityScale = 1;
        //     state.isJumping = true;
        // }
        // << pegar Naoki a la pared y que salte en dirección contraria 

        if (state.isRunning) {
            config.rigidbody2D.velocity = new Vector2(config.runningSpeed * state.direction, config.rigidbody2D.velocity.y);
        }
        else if (state.onGround) {
           config.rigidbody2D.velocity = new Vector2(0f, config.rigidbody2D.velocity.y);
        }

        if (state.isSlashing) {
            // config.rigidbody2D.velocity = new Vector2(0, config.rigidbody2D.velocity.y);
            return;
        }

        if (state.shouldJump && !state.canjumponwall) {
            state.shouldJump = false;
            state.isJumping = true;
            config.rigidbody2D.velocity = new Vector2(config.rigidbody2D.velocity.x, 0);
            config.rigidbody2D.AddForce(new Vector2(0, config.jumpSpeed));
        }

        if (state.shouldDoubleJump && !state.canjumponwall)
        {
            state.shouldDoubleJump = false;
            state.isDoubleJumping = true;
            config.rigidbody2D.velocity = new Vector2(config.rigidbody2D.velocity.x, 0);
            config.rigidbody2D.AddForce(new Vector2(0, config.jumpSpeed));
        }

        if (state.walljump) {
            state.shouldStickToWall = false;
            state.walljump = false;
            state.timerstart = true;
            state.isControllable = false;
            config.rigidbody2D.gravityScale = 1;
            state.direction *= -1;
            state.isJumping = true;
            config.rigidbody2D.AddForce(new Vector2(state.direction * config.forcejumpwallx, config.forcejumpwally));
        }

        //wall jump timer >>
        if (state.timerstart)
            config.timerwallinputaferjump -= Time.deltaTime;

        if (config.timerwallinputaferjump <= 0)
        {
            state.isJumping = true;
            state.isControllable = true;
            state.timerstart = false;
            state.canjumponwall = false;
            config.timerwallinputaferjump = 0.2f;
        }
        //<< wall jump timer

    }

    void Update () {

        //Detectar si está conectado un joystick >>
        string[] joystickconnected = Input.GetJoystickNames();
        for (int x = 0; x < joystickconnected.Length; x++)
        {
            // Debug.Log(joystickconnected[x]);
            if (joystickconnected[x].Length == 19)
            {
                //PS4 CONTROLLER IS CONNECTED
                state.joystick = true;
            }
            else if (joystickconnected[x].Length == 33)
            {
                //XBOX ONE CONTROLLER IS CONNECTED
                state.joystick = true;
            }
            else
            {
                state.joystick = true;
            }
        }
        //<< Detectar si está conectado un joystick

        if (state.isSliding) {
            Slide();
        }

        if (!state.isControllable) {
			state.isRunning = false;
			return;
		}
		// Receiving Damage blocks everything, being dead too
		if (state.isReceivingDamage) {
			return;
		}
		if (state.isDead) {
			// Destroy(config.armAnimator.gameObject);
			return;
		}
		// Dash
		if (state.isEnabledToDash && !state.isDashing && Input.GetButtonDown ("Dash")) {
			state.shouldDash = true;
			state.isJumping = false;
			state.isDoubleJumping = false;
			state.isEnabledToDash = false;
			return;
		}
		// Slash
		if (!state.isSlashing && !state.isThrowingShuriken && Input.GetButtonDown ("Slash")) {
			state.isSlashing = true;
			state.slashToApply = (state.slashToApply + 1) % 3 + 1;
			return;
		}
		// Throw Shuriken
		if (!state.isThrowingShuriken && config.gameManager.inventory.shuriken > 0 && Input.GetButtonDown ("Shuriken")) {
			state.shouldThrowShuriken = true;
		}

        // Special Attack
        if (!state.isDoingSpecialAttack && Input.GetButtonDown("SpecialAttack") && config.powerBarController.GetPowerAmount() >= 40) {
            state.isDoingSpecialAttack = true;
        }


        // Run
        /*
		if (Input.GetAxis("Horizontal") > 0) {
			state.direction = 1;
			state.isRunning = true;
		} else if (Input.GetAxis("Horizontal") < 0) {
			state.direction = -1;
			state.isRunning = true;
		} else {
			state.isRunning = false;
		}
        ///////////////////
        if (Input.GetKey("right"))
        {
            state.direction = 1;
            state.isRunning = true;
        }
        else if (Input.GetKey("left"))
        {
            state.direction = -1;
            state.isRunning = true;
        }
        else
        {
            state.isRunning = false;
        }*/
        //Run 
        if (Input.GetAxis("Horizontal") > 0 && state.joystick || Input.GetKey("right")) {
            state.direction = 1;
            state.isRunning = true;
        } else if (Input.GetAxis("Horizontal") < 0 && state.joystick || Input.GetKey("left")) {
            state.direction = -1;
            state.isRunning = true;
        } else {
            state.isRunning = false;
        }

        // Jump && DoubleJump
        state.onGround = IsOnGround();
		if (state.onGround) {
			state.isJumping = false;
			state.isDoubleJumping = false;
			state.isEnabledToDash = true;
            if (!bookKeeping.wasOnGround) {
                FXPlayer.MyRef.Play("caida");
            }
		}
        bookKeeping.wasOnGround = state.onGround;
		if (state.onGround && !state.isJumping && Input.GetButtonDown ("Jump")) {
			state.shouldJump = true;
		}
		if (!state.onGround && /*state.isJumping &&*/ !state.isDoubleJumping && Input.GetButtonDown ("Jump")) {
			state.shouldDoubleJump = true;
            state.isJumping = true;
		}

        if (Input.GetButtonDown("Jump") && state.canjumponwall)
            state.walljump = true;

    }

	public void InitSlide(Vector3 slideEnd) {
        if (!state.isEnabledToSlide) {
            return;
        }
		if (state.isSliding || state.shouldStopSliding) {
			return;
		}
		config.rigidbody2D.gravityScale = 0;
		config.rigidbody2D.velocity = Vector2.zero;
		state.isSliding = true;
		state.shouldStopSliding = false;
		state.isControllable = false;
		state.slideEnd = slideEnd;
		state.slideSpeedTween = DOTween.To(()=> state.currentSlideSpeed, x => state.currentSlideSpeed = x, config.slideSpeed, 2).SetEase(Ease.InSine);
	}

	void Slide() {
		if (state.shouldStopSliding) {
			EndSlide();
		}
		transform.position = Vector3.MoveTowards(transform.position, state.slideEnd, state.currentSlideSpeed);
	}

	void EndSlide() {
        state.isEnabledToSlide = false;
		config.rigidbody2D.gravityScale = 1;
		state.isSliding = false;
		state.shouldStopSliding = false;
		state.isControllable = true;
		state.slideSpeedTween.Kill();
		state.currentSlideSpeed = 0.1f;
        StartCoroutine(EnablePlayerToSlide());
	}

    IEnumerator EnablePlayerToSlide() {
        yield return new WaitForSeconds(1);
        state.isEnabledToSlide = true;
    }

	void LateUpdate() {
		Animate();
	}

    void Animate() {
        config.animator.SetBool("isDead", state.isDead);
        if (state.isDead) {
            config.animator.SetBool("isDamaged", false);
            return;
        }
        config.animator.SetBool("isDoingSpecialAttack", state.isDoingSpecialAttack);
        if (state.isDoingSpecialAttack) {
            return;
        }
        config.animator.SetBool("isDamaged", state.isReceivingDamage);
        if (state.isReceivingDamage) {
            return;
        }
        config.animator.SetBool("isDashing", state.isDashing);
        if (state.isDashing) {
            return;
        }
        
        // Slash
        if (state.isSlashing && (state.isRunning || state.onGround) && !state.isJumping && !state.isDoubleJumping) {
            config.armAnimator.SetBool("isSlashing", true);
            config.armAnimator.SetInteger("slashNumber", state.slashToApply - 1);
        } else if (state.isSlashing && (state.isJumping || state.isDoubleJumping)) {
            config.armAnimator.SetBool("isSlashingInvisible", true);
        } else {
            config.armAnimator.SetBool("isSlashing", false);
            config.armAnimator.SetBool("isSlashingInvisible", false);
        }

        if (state.isSliding) {
            if (state.currentSlideSpeed >= config.slideSpeedToChangeAnimation) {
                config.animator.SetBool("isStartingSliding", false);
                config.animator.SetBool("isDoingSliding", true);
            } else {
                config.animator.SetBool("isStartingSliding", true);
            }
        } else {
            config.animator.SetBool("isStartingSliding", false);
            config.animator.SetBool("isDoingSliding", false);
        }
        // Throw shuriken
        if (state.shouldThrowShuriken && (state.isRunning || state.onGround) && !state.isJumping && !state.isDoubleJumping) {
            config.armAnimator.SetTrigger("throwShuriken");
            state.shouldThrowShuriken = false;
            state.isThrowingShuriken = true;
        }
        if (state.shouldThrowShuriken && (state.isJumping || state.isDoubleJumping)) {
            config.armAnimator.SetTrigger("throwShurikenInvisible");
            state.shouldThrowShuriken = false;
            state.isThrowingShuriken = true;
        }
        // if ( // Cancel shuriken
        //         !state.isRunning &&
        //         !state.onGround &&
        //         !state.isJumping &&
        //         !state.isDoubleJumping
        //     ) {
        //     config.armAnimator.SetTrigger("cancelShuriken");
        //     state.isThrowingShuriken = false;
        // }

        if (state.direction != bookKeeping.previousDirection) {
            bookKeeping.previousDirection = state.direction;
            transform.Rotate(new Vector3(0, state.direction * 180, 0));
        }
        config.animator.SetBool("isRunning", state.isRunning && state.onGround);
        config.armAnimator.SetBool("isRunning", state.isRunning && state.onGround);
        config.animator.SetBool("isJumping", state.isJumping);
        config.animator.SetBool("isDoubleJumping", state.isDoubleJumping);
        config.animator.SetBool("isWallJumping", state.canjumponwall);
    }

	bool IsOnGround() {
		bool onGround = Physics2D.OverlapArea(
			new Vector2(transform.position.x - 0.2f - 1.2f/2f, transform.position.y - 1 - 1.1f/2f),
			new Vector2(transform.position.x - 0.2f + 1.2f/2f, transform.position.y - 1 - 1.1f - 0.1f),
			config.layerGround
		);
		return onGround;
	}

    // public void OnCollisionEnter2D(Collision2D collision){
    // 	// If it collides with an enemy, get damage
    // 	if (collision.gameObject.CompareTag("enemy")){
    // 		config.gameManager.SendMessage ("ReduceLife");
    // 	}
    // 	if (collision.gameObject.CompareTag("instant-death")){
    // 		config.gameManager.SendMessage ("Die");
    // 	}
    // } 

    public IEnumerator OnTriggerEnter2D(Collider2D collider) {
        yield return null;
        if (collider.CompareTag("wall")) {
            state.shouldStickToWall = true;
        }
        if ((collider.CompareTag("enemy") || collider.CompareTag("Damager")) && !state.isReceivingDamage && !state.shouldReceiveDamage && !state.isDoingSpecialAttack) {
            state.shouldReceiveDamage = true;
		}
        if (collider.CompareTag("DamagerThrower") && !state.isReceivingDamage && !state.shouldReceiveDamage && !state.isDoingSpecialAttack) {
            state.shouldBeThrownAway = true;
            state.shouldReceiveDamage = true;
        }
		if (collider.CompareTag("instant-death")){
			config.gameManager.Die();
		}
        if (collider.CompareTag("centipede-thrower-attack") && !state.shouldBeThrownAway) {
            state.shouldBeThrownAway = true;
            state.shouldReceiveDamage = true;
        }
	}

    public void OnTriggerExit2D(Collider2D collider) {
        if (collider.CompareTag("wall")) {
            state.shouldStickToWall = false;
        }
    }

	public void OnTriggerStay2D(Collider2D collider) {
		// if (collider.CompareTag("wire") && config.rigidbody2D.velocity.y < 0) {
		// 	InitSlide(collider.gameObject.GetComponent<WireController>().GetWireEndPosition());
		// }
		if (collider.CompareTag("wireEnd") && state.isSliding && state.isEnabledToSlide) {
			state.shouldStopSliding = true;
		}
	}

    public void NotifyInitShake() {
        if (state.onGround) {
            state.isControllable = false;
        }
    }

    public void NotifyEndShake() {
         state.isControllable = true;
    }

    public void Die() {
		state.isDead = true;
        config.armAnimator.gameObject.SetActive(false);
	}

	// public void ReceiveDamage() {
	// 	if (state.isReceivingDamage || state.isSlashing) {
	// 		return;
	// 	}
	// 	state.shouldReceiveDamage = true;
	// }

	public void StopReceivingDamage() {
		state.isReceivingDamage = false;
	}

	public void ThrowShurikenObject() {
		GameObject shuriken = Instantiate(config.shurikenPrephab);
		shuriken.transform.position = new Vector2(transform.position.x + 1.763f * state.direction, transform.position.y - 0.723f);
		shuriken.GetComponent<Rigidbody2D>().velocity = new Vector2(config.shurikenSpeed * state.direction, 0);
        config.gameManager.AddInventory("shuriken", -1);
        FXPlayer.MyRef.Play("throwShuriken");
	}

    public void EndThrowShurikenObject() {
        state.isThrowingShuriken = false;
    }

	public void DamageThroughSlash() {
		GameObject slashDamage = Instantiate(config.slashDamagePrephab);
		slashDamage.transform.position = new Vector2(transform.position.x + 1.5f * state.direction, transform.position.y -  0.6f);
        if (state.direction == 1) {
            slashDamage.transform.Rotate(new Vector3(0, -180f, 0));
        }
		slashDamage.transform.parent = transform;
		state.slashDamage = slashDamage;
        FXPlayer.MyRef.Play("slash" + state.slashToApply.ToString());
	}

	public void StopThrowingShuriken() {
		state.isThrowingShuriken = false;
	}

	public void StopSlashing() {
        Destroy(state.slashDamage);
		state.isSlashing = false;
	}

	public void StopDashing() {
		state.isDashing = false;
	}

    public void StartSpecialAttack() {
        config.rigidbody2D.gravityScale = 0;
        // config.rigidbody2D.velocity = Vector2.zero;
        config.rigidbody2D.velocity = new Vector2(3f * state.direction, 2f);
        config.powerBarController.IncreasePowerAmount(-40);
    }

    public void PerformSpecialAttackDamage() {
        config.rigidbody2D.velocity = new Vector2(20f * state.direction, 2f);
        config.SpecialAttackDamager.SetActive(true);
        FXPlayer.MyRef.Play("teleport");
    }

    public void StopSpecialAttack() {
        config.rigidbody2D.gravityScale = 1;
        config.rigidbody2D.velocity = Vector2.zero;
        state.isDoingSpecialAttack = false;
        config.SpecialAttackDamager.SetActive(false);
    }

	public void SetIsControllable(bool isControllable) {
		state.isControllable = isControllable;
	}


}
