﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NaokiBodyRenderHelper : MonoBehaviour {

	void Start(){
	}

	public void ThrowShurikenObject() {
		gameObject.GetComponentInParent<NaokiController>().ThrowShurikenObject();
	}
	
	public void EndThrowShurikenObject() {
		gameObject.GetComponentInParent<NaokiController>().EndThrowShurikenObject();
	}

	public void DamageThroughSlash() {
		gameObject.GetComponentInParent<NaokiController>().DamageThroughSlash();
	}

	public void StopDamashingThroughSlash() {
		gameObject.GetComponentInParent<NaokiController>().StopSlashing();
	}

	public void StartSpecialAttack() {
		gameObject.GetComponentInParent<NaokiController>().StartSpecialAttack();
	}

	public void PerformSpecialAttackDamage() {
		gameObject.GetComponentInParent<NaokiController>().PerformSpecialAttackDamage();
	}

	public void StopSpecialAttack() {
		gameObject.GetComponentInParent<NaokiController>().StopSpecialAttack();
	}

}
