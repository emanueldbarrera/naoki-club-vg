using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;

public class DialogController : MonoBehaviour {

    [SerializeField] NaokiController player;
    [SerializeField] GameManager gameManager;
    [SerializeField] Door door;

    [SerializeField] Animator characterAnimator;
    [SerializeField] Animator scrollAnimator;
    [SerializeField] Animator overlayAnimator;
    [SerializeField] Image character;
    [SerializeField] Image text;

    [SerializeField] Dialogable dialogableTopo;

    [SerializeField] int currentDialogScene;
    [SerializeField] int currentTextIndex = 0;
    [SerializeField] bool canShowNextText = false;

    [Serializable] class DialogScene {
        public Sprite character;
        public Sprite[] texts;
    }

    [SerializeField] DialogScene[] dialogScenes;

    // Start is called before the first frame update
    void Start() {
        text.sprite = null;
        Color color = text.color;
        color.a = 0;
        text.color = color;
    }

    // Update is called once per frame
    void Update() {
        if (canShowNextText && Input.GetButtonDown("Interact")) {
            ShowNextText();
        }
    }

    public void ShowDialog(int dialogIndex) {
        player.SetIsControllable(false);
        currentDialogScene = dialogIndex;
        scrollAnimator.SetTrigger("in");
        overlayAnimator.SetTrigger("in");
    }

    public void NotifyEndOfScrollIn() {
        character.sprite = dialogScenes[currentDialogScene].character;
        if (currentDialogScene == 1) { // COMPLETELY HARDCODED!!
            characterAnimator.SetTrigger("usagi-in");
        } else {
            characterAnimator.SetTrigger("in");
        }
    }

    public void NotifyEndOfCharacterIn() {
        currentTextIndex = 0;
        text.sprite = dialogScenes[currentDialogScene].texts[currentTextIndex];
        Color color = text.color;
        color.a = 255;
        text.color = color;
        canShowNextText = true;
    }

    void ShowNextText() {
        currentTextIndex++;
        if (currentTextIndex < dialogScenes[currentDialogScene].texts.Length) {
            text.sprite = dialogScenes[currentDialogScene].texts[currentTextIndex];
        } else {
            HideDialog();
        }
    }

    void HideDialog() {
        canShowNextText = false;
        if (currentDialogScene == 1) { // COMPLETELY HARDCODED!!
            characterAnimator.SetTrigger("usagi-out");
            gameManager.AddInventory("key-item", 0);
            dialogableTopo.dialogIndex = 2;
            dialogableTopo.dialogIndicator.SetActive(true);
        } else if (currentDialogScene == 2) {
            door.CloseDoorWithDelay();
            characterAnimator.SetTrigger("out");
        } else {
            characterAnimator.SetTrigger("out");
        }
        Color color = text.color;
        color.a = 0;
        text.color = color;
    }

    public void NotifyEndOfCharacterOut() {
        scrollAnimator.SetTrigger("out");
        overlayAnimator.SetTrigger("out");
        currentTextIndex = 0;
        player.SetIsControllable(true);
    }
}
