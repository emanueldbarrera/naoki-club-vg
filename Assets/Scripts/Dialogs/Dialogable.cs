﻿using UnityEngine;

public class Dialogable : MonoBehaviour {

    public MinerController miner;
    public DialogController dialogController;
    public GameObject dialogIndicator;
    public int dialogIndex;

    [SerializeField] bool canDialog = false;
    
    void Start() {
        
    }

    void Update() {
        if (canDialog && Input.GetButtonDown("Interact")) {
            canDialog = false;
            dialogIndicator.SetActive(false);
            dialogController.ShowDialog(dialogIndex);
            if (miner != null) {
                miner.StopForever();
            }
        }
    }

    public void OnTriggerEnter2D(Collider2D collider) {
        if (collider.CompareTag("Player")) {
            canDialog = true;
        }
    }

    public void OnTriggerExit2D(Collider2D collider) {
        if (collider.CompareTag("Player")) {
            canDialog = false;
        }
    }
}
