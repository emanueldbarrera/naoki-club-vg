﻿using UnityEngine;
using System;
using UnityEngine.U2D;

public class DialogTempController : MonoBehaviour {

    [Serializable] class Config {
        public GameObject dialog;
        public SpriteAtlas textAtlas;
        public SpriteAtlas nameAtlas;
        public SpriteAtlas characterAtlas;
        public SpriteRenderer textRenderer;
        public SpriteRenderer nameRenderer;
        public SpriteRenderer characterRenderer;
    }

    [Serializable] class State {
        public int currentDialog = 1;
    }

    [SerializeField] Config config;
    [SerializeField] State state;
    
    void Start() {
    }

    
    void Update() {
        if (config.dialog.activeSelf && Input.GetKeyDown(KeyCode.R)) {
            // Close
            if (state.currentDialog == 3 || state.currentDialog == 6 || state.currentDialog == 9) {
                config.dialog.SetActive(false);
            }
            state.currentDialog++;
            config.textRenderer.sprite = config.textAtlas.GetSprite(state.currentDialog.ToString());
            if (state.currentDialog == 4) {
                config.nameRenderer.sprite = config.nameAtlas.GetSprite("usagi");
                config.characterRenderer.sprite = config.characterAtlas.GetSprite("usagi-char");
            }
            if (state.currentDialog == 7) {
                config.nameRenderer.sprite = config.nameAtlas.GetSprite("moku");
                config.characterRenderer.sprite = config.characterAtlas.GetSprite("moku-char");
            }
        }
    }

    public void ShowDialog() {
        if (config.dialog.activeSelf) return;
        config.dialog.SetActive(true);
    }
}
