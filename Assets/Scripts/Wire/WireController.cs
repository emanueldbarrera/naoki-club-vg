﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WireController : MonoBehaviour {

    [SerializeField] GameObject wireEnd;

    // Start is called before the first frame update
    void Start() {
        
    }

    // Update is called once per frame
    void Update() {
        
    }

    public Vector3 GetWireEndPosition() {
        return wireEnd.transform.position;
    }
}
