﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	[SerializeField] CameraController cameraController;
	[SerializeField] Animator overlayAnimator;
	[SerializeField] Animator menuAnimator;
	[SerializeField] Animator victoryMenuAnimator;
	[SerializeField] MappedImageUtility lifeUI;
	[SerializeField] CoinsController coinsUI;
	[SerializeField] MappedImageUtility shurikenUnitUI;
	[SerializeField] MappedImageUtility shurikenTenUI;
	[SerializeField] GameObject keyItem;
	[SerializeField] GameObject potionItem;
	[SerializeField] GameObject UI;
	[SerializeField] Animator barsAnimator;
	[SerializeField] BabaController baba;
	[SerializeField] NaokiController player;
	[SerializeField] CentipedeController centipede;
	
	public int life = 10;
	int previousLife;
	string cinematicActive;

	[Serializable] public class Inventory {
		public int coins = 0;
		public int previousCoins;
		public int shuriken = 0;
		public bool keyItem = false;
		public bool potion = false;
	}
	[SerializeField] public Inventory inventory;

	[SerializeField] CinemachineVirtualCamera camera1;
	[SerializeField] CameraController mainCamera;

	// [SerializeField] AudioSource audio1;
	// [SerializeField] AudioSource audio2;
	// [SerializeField] AudioSource audio3;
	[SerializeField] AudioController audioBioma1;
	[SerializeField] AudioController audioBioma3;
	[SerializeField] AudioController audioBoss;

	AudioSource activeAudio;
	AudioSource nextActiveAudio;

	public bool isLevel2Active = false;

	bool isCrossfadingMusic = false;
	float volume = 1f;

	public static GameManager MyRef { get; private set; }
    void Awake() { MyRef = this; }

	// Use this for initialization
	void Start () {
		previousLife = life;
		inventory.previousCoins = inventory.coins;
		InitLevel1();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.R)) {
			SceneManager.LoadScene ("level_0");
		}
		if (Input.GetKey(KeyCode.Escape)) {
			Application.Quit();
		}
		if (Input.GetAxis("Heal") > 0 && inventory.potion && life < 20 && life > 0) {
			potionItem.SetActive(false);
			inventory.potion = false;
			life += 10;
		}
		if (isCrossfadingMusic) CrossfadeMusic();

		// Update life UI
		if (life != previousLife) {
			lifeUI.SetIndexNumber(life);
			previousLife = life;
		}

		// Update life UI
		if (inventory.coins != inventory.previousCoins) {
			coinsUI.SetCoins(inventory.coins);
			inventory.previousCoins = inventory.coins;
		}
	}

	public void AddInventory(string item, int amount) {
		switch(item){
			case "coins":
				inventory.coins += amount;
				break;
			case "key-item":
				inventory.keyItem = true;
				keyItem.SetActive(true);
				break;
			case "shuriken":
				inventory.shuriken += amount;
				if (inventory.shuriken > 99)
					inventory.shuriken = 99;
				if (inventory.shuriken < 0)
					inventory.shuriken = 0;
				shurikenUnitUI.SetIndexNumber(inventory.shuriken % 10);
				shurikenTenUI.SetIndexNumber((int)(inventory.shuriken / 10));
				break;
			case "potion":
				inventory.potion = true;
				potionItem.SetActive(true);
				break;
		}
	}

	void CrossfadeMusic(){
		volume = volume - 0.05f;
		activeAudio.volume = volume;
		if (volume <= 0) {
			activeAudio.Stop();
			activeAudio = nextActiveAudio;
			activeAudio.Play();
			isCrossfadingMusic = false;
			volume = 1f;
			activeAudio.volume = volume;
		}
	}

	public void InitLevel1() {
		// player.transform.position = new Vector2(920.3f, 13.8f);
		// player.transform.Rotate(new Vector3(0, 180, 0));
		camera1.enabled = true;
		// camera2.enabled = false;
		// mainCamera.activateLimits1 = true;
		// mainCamera.activateLimits2 = false;
		// activeAudio = audio1;
		audioBioma1.Play();
		
	}

	public void InitLevel2() {
		if (isLevel2Active)
			return;	
		isLevel2Active = true;
		overlayAnimator.SetTrigger("inFull");
		StartCoroutine(ScheduleInitLevel2());
	}

	IEnumerator ScheduleInitLevel2() {
		yield return new WaitForSeconds(1);
		cameraController.SetLevelCameraConfigActive(1);
		player.transform.position = new Vector2(1258.5f, 8.25f);
		overlayAnimator.SetTrigger("outFull");
		audioBioma1.Stop();
		audioBioma3.Play();
		// isCrossfadingMusic = true;
		// volume = 0f;
	}

	public void ReduceLife() {
		life--;
		if (life <= 0) {
			// Die
			player.SendMessage ("Die");
			Die();
		} else {
			// player.SendMessage ("ReceiveDamage");

		}
	}

	public void Die() {
		life = 0;
		lifeUI.SendMessage("SetIndexNumber", life); // Update UI animation
		player.SendMessage ("Die");
		StartCoroutine(ScheduleFinalMenu());
	}

	IEnumerator ScheduleFinalMenu() {
		yield return new WaitForSeconds(0.2f);
		overlayAnimator.SetTrigger("in");
		menuAnimator.SetTrigger("show");
	}

	public void ShowVictoryMessage() {
		player.SetIsControllable(false);
		overlayAnimator.SetTrigger("in");
		victoryMenuAnimator.SetTrigger("show");
	}

	/***********************
		Boss fight
	************************/
	public void InitBossFight() {
		audioBioma3.Stop();
		FXPlayer.MyRef.Play("emerge");
		// Freeze Naoki
		player.SetIsControllable(false);
		// Make centipede emerge
		barsAnimator.SetTrigger("BarsIn");
		cinematicActive = "centipede";

		// Music
		// nextActiveAudio = audio2;
		// isCrossfadingMusic = true;
		// volume = 0f;
	}

	IEnumerator CentipedeEmerge() {
		ShakeWorld(8, false);
		yield return new WaitForSeconds(6.5f);
		centipede.Emerge();
	}

	public void EndOfEmerge() {
		// Unfreeze Naoki
		barsAnimator.SetTrigger("BarsOut");

		// Music
		// nextActiveAudio = audio3;
		// isCrossfadingMusic = true;
		// volume = 0f;
		// UI
		UI.SetActive(true);
		UI.transform.position = new Vector2(-82.6f, 32.3f);
		UI.transform.localScale = new Vector2(4f, 4f);
		audioBoss.Play();
	}

	/***********************
		Shakes behavior
	************************/
	public void ShakeWorld(float length, bool freezePlayer) {
		mainCamera.Shake();
		if (freezePlayer) {
			player.NotifyInitShake();
			StartCoroutine(ScheduleEndOfFreezingBecauseOfShake());
		}
		StartCoroutine(ScheduleEndShake(length));
	}

	IEnumerator ScheduleEndOfFreezingBecauseOfShake() {
		yield return new WaitForSeconds(1);
		player.NotifyEndShake();
	}

	IEnumerator ScheduleEndShake(float length) {
		yield return new WaitForSeconds(length);
		mainCamera.EndShake();
	}

	/***********************
		Boss fight: behavior when traspassing boss' baba
	************************/
	public void InitBaba() {
		// Freeze player
		player.SetIsControllable(false);

		// Show bars
		barsAnimator.SetTrigger("BarsIn");
		cinematicActive = "baba";
		audioBioma3.startClip.volume = 0.3f;
		audioBioma3.loopClip.volume = 0.3f;
	}

	public void NotifyEndOfBarsShow() {
		switch(cinematicActive) {
			case "baba":
				// Enable baba
				StartCoroutine(InitBabaWithDelay());
				break;
			case "centipede":
				StartCoroutine(CentipedeEmerge());
				break;
		}
	}

	IEnumerator InitBabaWithDelay() {
		yield return new WaitForSeconds(1);
		baba.InitBaba();
	}

	public void NotifyEndOfInitBaba() {
		// Hide bars
		StartCoroutine(HideBarsWithDelay());
	}

	IEnumerator HideBarsWithDelay() {
		yield return new WaitForSeconds(1);
		barsAnimator.SetTrigger("BarsOut");
		audioBioma3.startClip.volume = 1f;
		audioBioma3.loopClip.volume = 1f;
	}

	public void NotifyEndOfBarsHide() {
		// Unfreeze player
		player.SetIsControllable(true);
	}
}
