﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevatorHandgripController : MonoBehaviour {

    [SerializeField] ElevatorController elevator;

    Animator animator;
    bool isMovingGrip = false;

    void Start() {
        animator = GetComponent<Animator>();
    }

    void OnTriggerEnter2D(Collider2D collider) {
        if (!isMovingGrip && collider.CompareTag("slash")) {
            StartMoveGrip();
        }
    }

    void StartMoveGrip() {
        isMovingGrip = true;
        animator.SetTrigger("move");
    }

    public void ActivateMovement() {
        elevator.Activate();
    }

    public void EndMoveGrip() {
        isMovingGrip = false;
    }

}
