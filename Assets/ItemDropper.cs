﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;


public class ItemDropper : MonoBehaviour {


    [Serializable] class GenerableItem {
        public GameObject itemPrefab;
        public int itemMin;
        public int itemMax;

    }

    [SerializeField] float itemImpulseY;
    [SerializeField] float itemImpulseX;
    [SerializeField] GenerableItem[] items;
    
    public void DropItems() {
        foreach (GenerableItem item in items) {
            int quantityItems = (int)UnityEngine.Random.Range(item.itemMin, item.itemMax + 1);
            for (int i = 0; i < quantityItems; i++) {
                GameObject go = Instantiate(item.itemPrefab);
		        go.transform.position = transform.position;
                go.GetComponent<Rigidbody2D>().AddForce(
                    new Vector2(UnityEngine.Random.Range(-itemImpulseX, itemImpulseX), itemImpulseY)
                );
            }
        }
    }

}
