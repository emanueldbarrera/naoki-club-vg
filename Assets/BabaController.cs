﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BabaController : MonoBehaviour {

    [SerializeField] GameManager manager;
    [SerializeField] Animator animator;
    [SerializeField] GameObject babaCollider;

    bool isBabaLive = false;
    
    void Start() {
        animator = GetComponent<Animator>();
    }

    void Update() {
        
    }

    void OnTriggerEnter2D(Collider2D collider) {
        if (!isBabaLive && collider.CompareTag("Player")) {
            manager.InitBaba();
        }
    }

    public void InitBaba() {
        animator.SetTrigger("init");
        babaCollider.SetActive(true);
    }

    public void NotifyEndOfInitBaba() {
        manager.NotifyEndOfInitBaba();
        isBabaLive = true;
    }
}
