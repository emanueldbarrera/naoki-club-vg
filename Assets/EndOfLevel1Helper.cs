﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndOfLevel1Helper : MonoBehaviour {

    [SerializeField] GameManager gameManager;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter2D(Collider2D collider) {
        if (collider.CompareTag("Player")) {
            gameManager.InitLevel2();
        }
    }
}
