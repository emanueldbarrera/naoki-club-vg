﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MessageController : MonoBehaviour {
    
    [SerializeField] bool shouldRestart = false;

    // Update is called once per frame
    void Update() {
        if (shouldRestart && Input.GetButtonDown("Slash")) {
		    SceneManager.LoadScene("level_0");
        }
    }

    public void EnableRestart() {
        shouldRestart = true;
    }
}
