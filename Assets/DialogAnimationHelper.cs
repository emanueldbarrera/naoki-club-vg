﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogAnimationHelper : MonoBehaviour {

    [SerializeField] DialogController dialogController;

    public void NotifyEndOfScrollIn() {
        dialogController.NotifyEndOfScrollIn();
    }

    public void NotifyEndOfCharacterIn() {
        dialogController.NotifyEndOfCharacterIn();
    }

    public void NotifyEndOfCharacterOut() {
        dialogController.NotifyEndOfCharacterOut();
    }

}
