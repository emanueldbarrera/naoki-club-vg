﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EstalactitaController : MonoBehaviour {

    float initialPositionY = 27f;
    float finalPositionY = -10f;

    float minPositionX = -35f;
    float maxPositionX = 2;
    
    void Start() {
        transform.localPosition = new Vector3(
            UnityEngine.Random.Range(minPositionX, maxPositionX),
            initialPositionY,
            transform.position.z
        );
    }

    void Update() {
        if (transform.localPosition.y <= finalPositionY) { //hardcode
            transform.localPosition = new Vector3(
                UnityEngine.Random.Range(minPositionX, maxPositionX),
                initialPositionY,
                transform.localPosition.z
            );
            gameObject.SetActive(false);
        }
    }
}
