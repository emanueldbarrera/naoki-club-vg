﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinsController : MonoBehaviour {

    [SerializeField] UnityEngine.UI.Text text;

    public void SetCoins(int coins) {
        text.text = coins.ToString();
    }
}
