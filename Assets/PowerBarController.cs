﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerBarController : MonoBehaviour {

    [SerializeField] int powerAmountMax = 40;
    [SerializeField] int powerAmount = 0;
    [SerializeField] Image mask;
    [SerializeField] GameObject reflex;
    float initialTop;
    float initialHeight;

    public static PowerBarController MyRef { get; private set; }
    void Awake() { MyRef = this; }

    // Start is called before the first frame update
    void Start() {
        initialTop = mask.rectTransform.offsetMax.y;
        initialHeight = mask.rectTransform.rect.height;
    }

    // Update is called once per frame
    void Update() {
        mask.rectTransform.offsetMax = new Vector2(
            mask.rectTransform.offsetMax.x,
            initialTop - initialHeight + ( ((float)powerAmount/(float)powerAmountMax) * initialHeight)
        );
        if (powerAmount == powerAmountMax)
            reflex.SetActive(true);
        else
            reflex.SetActive(false);
    }

    public void IncreasePowerAmount(int powerUnits) {
        int newPowerAmount = powerAmount + powerUnits;
        if (newPowerAmount <= powerAmountMax && newPowerAmount >= 0) {
            powerAmount += powerUnits;
        }
        if (newPowerAmount > powerAmountMax) {
            powerAmount = powerAmountMax;
        }
        if (newPowerAmount < 0) {
            powerAmount = 0;
        }
    }

    public int GetPowerAmount() {
        return powerAmount;
    }
}
