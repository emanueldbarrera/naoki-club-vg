﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CentipedeBiteColliderController : MonoBehaviour {

    [SerializeField] CentipedeController centipede;

    void Start() {
        
    }

    void Update() {
        
    }

    void OnTriggerStay2D(Collider2D collider) {
        if (collider.CompareTag("Player")) {
            centipede.Bite();
        }
    }
}
