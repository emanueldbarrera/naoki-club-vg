﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarsController : MonoBehaviour {
    
    [SerializeField] GameManager manager;
    // [SerializeField] GameObject top;
    // [SerializeField] GameObject bottom;

    // void Start() {
    //     top.SetActive(true);
    //     bottom.SetActive(true);
    // }

    public void EndOfBarsShow() {
        manager.NotifyEndOfBarsShow();
    }

    public void EndOfBarsHide() {
        manager.NotifyEndOfBarsHide();
    }
}
