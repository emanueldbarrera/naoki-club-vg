﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevatorController : MonoBehaviour {

    // Config
    [SerializeField] float speed;
    [SerializeField] Transform elevatorObject;
    [SerializeField] Transform weightObject;
    [SerializeField] Transform elevatorMin;
    [SerializeField] Transform elevatorMax;
    [SerializeField] Transform weightObjectMin;
    [SerializeField] Transform weightObjectMax;

    // State
    [SerializeField] bool isMoving = true;
    [SerializeField] int direction = 1; // 1 => up, -1 => down
    [SerializeField] Transform currentElevatorTarget;
    [SerializeField] Transform currentWeightObjectTarget;
    [SerializeField] AudioSource audio;
    [SerializeField] bool canPlaySound = false;

    Vector3 previous;
    float velocity;
    [SerializeField] bool audioPlaying = false;

    void Start() {
        StartCoroutine(SoundDelay());
        currentElevatorTarget = elevatorMax;
        currentWeightObjectTarget = weightObjectMax;
        previous = elevatorObject.transform.position;
    }

    void Update() {
        velocity = ((elevatorObject.transform.position - previous).magnitude) / Time.deltaTime;
        previous = elevatorObject.transform.position;
        if (Mathf.Abs(velocity) > 0) {
            if (!audioPlaying && canPlaySound) {
                audio.Play();
                audioPlaying = true;
            }
        } else {
            if (audioPlaying) {
                audio.Stop();
                audioPlaying = false;
            }
        }

        if (isMoving) {
            elevatorObject.position = new Vector2(
                elevatorObject.position.x,
                Vector2.MoveTowards(elevatorObject.position, currentElevatorTarget.position, speed * Time.deltaTime).y
            );
            weightObject.position = new Vector2(
                weightObject.position.x,
                Vector2.MoveTowards(weightObject.position, currentWeightObjectTarget.position, speed * Time.deltaTime).y
            );
        }
    }

    IEnumerator SoundDelay() {
        yield return new WaitForSeconds(3);
        canPlaySound = true;
    }

    public void Activate() {
        if (currentElevatorTarget == elevatorMin) {
            currentElevatorTarget = elevatorMax;
        } else {
            currentElevatorTarget = elevatorMin;
        }
        if (currentWeightObjectTarget == weightObjectMin) {
            currentWeightObjectTarget = weightObjectMax;
        } else {
            currentWeightObjectTarget = weightObjectMin;
        }
        FXPlayer.MyRef.Play("palanca");
    }
}
