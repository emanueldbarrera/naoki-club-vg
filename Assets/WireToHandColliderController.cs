﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WireToHandColliderController : MonoBehaviour {

    [SerializeField] NaokiController player;

    void OnTriggerStay2D(Collider2D collider) {
        if (collider.CompareTag("wire")) {
            player.InitSlide(collider.gameObject.GetComponent<WireController>().GetWireEndPosition());
        }
    }
}
