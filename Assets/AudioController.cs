﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController: MonoBehaviour {


    public AudioSource startClip;
    public AudioSource loopClip;
    private bool shouldStartIntro = false;

    void Update() {
        if (shouldStartIntro) {
            shouldStartIntro = false;
            PlaySound();
        }
    }

    void PlaySound() {
        startClip.Play();
        loopClip.PlayDelayed(startClip.clip.length);
    }

    public void Play() {
        shouldStartIntro = true;
    }

    public void Stop() {
        startClip.Stop();
        loopClip.Stop();
    }
 }