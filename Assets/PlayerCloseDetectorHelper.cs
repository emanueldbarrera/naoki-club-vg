﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCloseDetectorHelper : MonoBehaviour {

    [SerializeField] BanditController bandit;

    void OnTriggerStay2D(Collider2D collider) {
        if (collider.CompareTag("Player")) {
            bandit.OnPlayerStayVeryClose();
        }
    }
}
