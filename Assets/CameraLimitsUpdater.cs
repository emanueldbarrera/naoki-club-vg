﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLimitsUpdater : MonoBehaviour {

    [SerializeField] CameraController cameraController;
    [SerializeField] LevelCameraConfig levelCameraConfig;

    void OnTriggerEnter2D(Collider2D collider) {
        if (collider.CompareTag("Player")) {
            cameraController.UpdateLevelCameraConfigActive(levelCameraConfig);
        }
    }

}
