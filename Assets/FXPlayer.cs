﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FXPlayer : MonoBehaviour {

    public static FXPlayer MyRef { get; private set; }
    void Awake() { MyRef = this; }

    [SerializeField] AudioSource throwShuriken;
    [SerializeField] AudioSource shurikenImpact;
    [SerializeField] AudioSource slash;
    [SerializeField] AudioSource slash1;
    [SerializeField] AudioSource slash2;
    [SerializeField] AudioSource slash3;
    [SerializeField] AudioSource dash;
    [SerializeField] AudioSource box1;
    [SerializeField] AudioSource box2;
    [SerializeField] AudioSource hit;
    [SerializeField] AudioSource palanca;
    [SerializeField] AudioSource caida;
    [SerializeField] AudioSource teleport;
    [SerializeField] AudioSource emerge;
    [SerializeField] AudioSource tirarAcido;
    [SerializeField] AudioSource slashCut;
    [SerializeField] AudioSource bite;
    [SerializeField] AudioSource swoosh;
    [SerializeField] AudioSource centipedeDamage;
    [SerializeField] AudioSource moan;
    
    public void Play(string sound) {
        switch (sound) {
            case "throwShuriken":
                throwShuriken.Play();
                break;
            case "shurikenImpact":
                shurikenImpact.Play();
                break;            
            case "slash":
                slash.Play();
                break;
            case "slash1":
                slash1.Play();
                break;
            case "slash2":
                slash2.Play();
                break;
            case "slash3":
                slash3.Play();
                break;
            case "dash":
                dash.Play();
                break;
            case "box1":
                box1.Play();
                break;
            case "box2":
                box2.Play();
                break;
            case "palanca":
                // palanca.Play();
                break;
            case "caida":
                // caida.Play();
                break;
            case "teleport":
                teleport.Play();
                break;
            case "hit":
                hit.Play();
                break;
            case "emerge":
                emerge.Play();
                break;
            case "tirarAcido":
                tirarAcido.Play();
                break;
            case "slashCut":
                if (slashCut.isPlaying)
                    return;
                slashCut.time = 0.5f;
                slashCut.Play();
                break;
            case "bite":
                if (bite.isPlaying)
                    return;
                bite.Play();
                break;
            case "swoosh":
                swoosh.Play();
                break;
            case "moan":
                moan.Play();
                break;
        }
    }
}
