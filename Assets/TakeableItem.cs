﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeableItem : MonoBehaviour {

    [SerializeField] GameManager gameManager;
    [SerializeField] string itemName;
    [SerializeField] int minAmount;
    [SerializeField] int maxAmount;
    bool taken = false;

    void Start() {
        gameManager = GameManager.MyRef;
    }

    // Update is called once per frame
    void Update() {
    }

    void OnTriggerEnter2D(Collider2D collider) {
        if (taken) return;
        if (collider.CompareTag("Player")) {
            taken = true;
            int amount = (int)UnityEngine.Random.Range(minAmount, maxAmount + 1);
            gameManager.AddInventory(itemName, amount);
            Destroy(transform.parent.gameObject);
        }
    }
}
